/* Otro comentario */

package appburbuja;

public class Burbuja {
   private final int nItems;
   private final int[] items;
   private int indice;

    public Burbuja(int nItems) {
        items = new int[nItems];
        this.nItems = nItems;
        indice = 0;
    }
   public void addItem(int numero){
       if(indice<nItems){
           items[indice]= numero;
           indice++;
       }
   }
   
   public void print(){
       for (int item : items) {
           System.out.println(item);           
       }
   }
   
   public void sort(){
       int aux;
       for (int i = 0; i < items.length - 1; i++) {
           for (int j = 0; j < items.length- i - 1; j++) {
              if(items[j+1] < items[j] ){
                 aux = items[j+1];
                 items[j+1]=items[j];
                 items[j]=aux;
              }   
           }           
       }
   }
   
   
}
