/* Nuevo comentario 
*/
package appburbuja;

import java.util.Scanner;

public class AppBurbuja {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        System.out.print("Introduce el número de items: ");
        int n = in.nextInt();
        Burbuja b = new Burbuja(n);
        System.out.flush();
        for(int i=0;i<n;i++){
            System.out.print("Dame el número de la posición " + (i+1)+" :");
            int num = in.nextInt();
            System.out.flush();
            b.addItem(num);
        }
        System.out.println("Antes de ordenar");
        b.print();
        b.sort();
        System.out.println("Después de ordenar");
        b.print();
        
    }
    
}
